package com.kfwebstandard.util;

import java.util.PropertyResourceBundle;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;

/**
 * Retrieving a resource bundle from an injectable class Assumes that the var
 * name of the bundle is msgs Based on
 * https://stackoverflow.com/questions/13655540/read-resource-bundle-properties-in-a-managed-bean
 *
 * @author Ken Fogel
 */
public class BundleProducer {

    @Produces
    public PropertyResourceBundle getBundle() {
        FacesContext context = FacesContext.getCurrentInstance();
        return context.getApplication().evaluateExpressionGet(context, "#{msgs}", PropertyResourceBundle.class);
    }

}
