package com.kfwebstandard.beans;

import java.io.Serializable;

public class CreditCard implements Serializable {

    private final String number;

    public CreditCard(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return number;
    }
}
